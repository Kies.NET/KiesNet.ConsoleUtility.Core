﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KiesNet.ConsoleUtility.Core
{
    public static class ConsoleUtility
    {
        public static void WriteColored(string text, ConsoleColor foregroundColor, ConsoleColor backgroundColor)
        {
            ConsoleColor currentForegroundColor = Console.ForegroundColor;
            ConsoleColor currentBackgroundColor = Console.BackgroundColor;

            Console.ForegroundColor = foregroundColor;
            Console.BackgroundColor = backgroundColor;

            Console.Write(text);

            Console.ForegroundColor = currentForegroundColor;
            Console.BackgroundColor = currentBackgroundColor;
        }

        public static void WriteColored(string text, ConsoleColor foregroundColor)
        {
            WriteColored(text, foregroundColor, Console.BackgroundColor);
        }

        public static void WriteLineColored(string text, ConsoleColor foregroundColor, ConsoleColor backgroundColor)
        {
            WriteColored(text + Environment.NewLine, foregroundColor, backgroundColor);
        }

        public static void WriteLineColored(string text, ConsoleColor foregroundColor)
        {
            WriteColored(text + Environment.NewLine, foregroundColor);
        }

        public static void WriteCentered(string text)
        {
            Console.CursorLeft = Console.WindowWidth / 2 - text.Length / 2;
            Console.Write(text);
        }

        public static void WriteLineCentered(string text)
        {
            WriteCentered(text);
            Console.WriteLine();
        }

        public static void WriteRightAligned(string text)
        {
            Console.CursorLeft = Console.WindowWidth - text.Length;
            Console.Write(text);
        }

        public static void WriteLineRightAligned(string text)
        {
            WriteRightAligned(text);
            Console.WriteLine();
        }

        public static void Underline(string text, char lineChar = '-')
        {
            Console.WriteLine(text);
            Console.WriteLine(new string(lineChar, text.Length));
        }

        public static void WriteWithBorder(string text, char borderChar = '*')
        {
            Console.WriteLine(new string(borderChar, text.Length + 4));
            Console.WriteLine(borderChar + " "  + text + " " + borderChar);
            Console.WriteLine(new string(borderChar, text.Length + 4));
        }

        public static string ReadLine(string description = null)
        {
            if (!string.IsNullOrEmpty(description))
                Console.Write(description + ": ");

            return Console.ReadLine();
        }

        public static T ReadLine<T>(string description = null, string errorMessage = "Error")
        {
            T result = default(T);
            bool resultValid = false;

            while(!resultValid)
                try
                {
                    string input = ReadLine(description);
                    result = (T)Convert.ChangeType(input, typeof(T));

                    resultValid = true;
                }
                catch (FormatException)
                {
                    resultValid = false;

                    WriteLineColored(errorMessage + Environment.NewLine, ConsoleColor.Red);
                }

            return result;
        }

        public static void SkipLines(int count)
        {
            Console.Write(new string('\n', count));
        }

        public static void SkipSpaces(int count)
        {
            Console.Write(new string(' ', count));
        }
    }
}
