﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KiesNet.ConsoleUtility.Core
{
    public class ConsoleMenuItem
    {
        internal enum ItemType
        {
            Default,
            Back
        }

        public string Title { get; set; }

        public Action ItemSelected { get; set; }

        internal ItemType Type { get; set; }

        public ConsoleMenuItem(string title)
        {
            this.Title = title;
        }

        public override string ToString()
        {
            return this.Title;
        }
    }
}
