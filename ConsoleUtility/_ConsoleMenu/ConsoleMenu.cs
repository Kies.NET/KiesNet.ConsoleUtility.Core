﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KiesNet.ConsoleUtility.Core
{
    public class ConsoleMenu : ConsoleMenuItem
    {
        public List<ConsoleMenuItem> MenuItems { get; set; }

        public string ItemBullet { get; set; } = "->";
        public string MenuBullet { get; set; } = "==>";
        public bool ShowTitle { get; set; } = true;

        public ConsoleMenu(List<ConsoleMenuItem> menuItems, string title) : base(title)
        {
            this.MenuItems = menuItems;
        }

        public ConsoleMenu(string title) : this(new List<ConsoleMenuItem>(), title)
        {
        }
        
        public ConsoleMenuItem Show()
        {
            if (!this.MenuItems.Any())
                return null;
            
            ConsoleColor currentForegroundColor = Console.ForegroundColor;
            ConsoleColor currentBackgroundColor = Console.BackgroundColor;

            // Add back entry for submenus
            List<ConsoleMenu> submenus = this.MenuItems.Where(item => item is ConsoleMenu)
                                                       .Cast<ConsoleMenu>()
                                                       .ToList();

            foreach (ConsoleMenu menu in submenus)
                menu.MenuItems.Add(new ConsoleMenuItem("Zurück")
                {
                    Type = ItemType.Back
                });

            int selectionIndex;
            ConsoleMenuItem selectedItem;
            ConsoleMenuItem selectedSubMenuItem;

            // Logic to select items
            do
            {
                Console.CursorVisible = false;
                int itemCursorTop;
                selectionIndex = 0;
                selectedItem = this.MenuItems.First();
                ConsoleKey key = 0;

                // Show items
                Console.Clear();

                if (this.ShowTitle)
                {
                    ConsoleUtility.Underline(this.Title);
                    ConsoleUtility.SkipLines(1);
                }

                itemCursorTop = Console.CursorTop;

                Console.ForegroundColor = currentBackgroundColor;
                Console.BackgroundColor = currentForegroundColor;
                foreach (ConsoleMenuItem item in this.MenuItems)
                {
                    this.RenderMenuItem(item);
                    Console.ForegroundColor = currentForegroundColor;
                    Console.BackgroundColor = currentBackgroundColor;
                }
                
                selectedSubMenuItem = null;

                do
                {
                    key = Console.ReadKey(true).Key;

                    if (key == ConsoleKey.DownArrow 
                            || key == ConsoleKey.UpArrow)
                    {
                        Console.SetCursorPosition(0, itemCursorTop + selectionIndex);
                        this.RenderMenuItem(this.MenuItems[selectionIndex]);

                        Console.ForegroundColor = currentBackgroundColor;
                        Console.BackgroundColor = currentForegroundColor;

                        if (key == ConsoleKey.DownArrow
                                && selectionIndex < this.MenuItems.Count - 1)
                            ++selectionIndex;

                        else if (key == ConsoleKey.UpArrow
                                    && selectionIndex > 0)
                            --selectionIndex;

                        Console.SetCursorPosition(0, itemCursorTop + selectionIndex);
                        this.RenderMenuItem(this.MenuItems[selectionIndex]);

                        Console.ForegroundColor = currentForegroundColor;
                        Console.BackgroundColor = currentBackgroundColor;

                        selectedItem = this.MenuItems[selectionIndex];
                    }
                }
                while (key != ConsoleKey.Enter);

                selectedItem.ItemSelected?.Invoke();

                if (selectedItem is ConsoleMenu)
                    selectedSubMenuItem = ((ConsoleMenu)this.MenuItems[selectionIndex]).Show();

            }
            while (selectedSubMenuItem != null && selectedSubMenuItem.Type == ItemType.Back);
            
            if (selectedSubMenuItem != null && selectedSubMenuItem.Type != ItemType.Back)
                selectedItem = selectedSubMenuItem;

            // Remove back button of submenus
            submenus.ForEach(menu => menu.MenuItems.Remove(menu.MenuItems.Last()));

            Console.Clear();
            Console.CursorVisible = true;

            return selectedItem;
        }

        public void AddEntry(ConsoleMenuItem item)
        {
            this.MenuItems.Add(item);
        }

        public void RemoveEntry(ConsoleMenuItem item)
        {
            this.MenuItems.Remove(item);
        }

        private void RenderMenuItem(ConsoleMenuItem item)
        {
            if (item is ConsoleMenu
                    && !string.IsNullOrEmpty(this.MenuBullet))
                Console.Write(this.MenuBullet + " ");

            else if (!string.IsNullOrEmpty(this.ItemBullet))
                Console.Write(this.ItemBullet + " ");

            Console.WriteLine(item);
        }
    }
}
